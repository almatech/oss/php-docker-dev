FROM php:8.2-fpm-alpine

WORKDIR /var/www/app

RUN apk add --no-cache --update bash gmp-dev icu-dev libzip-dev libpng-dev gnu-libiconv \
    && apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
    && docker-php-ext-install zip \
    && wget https://github.com/FriendsOfPHP/pickle/releases/latest/download/pickle.phar \
    && mv pickle.phar /usr/local/bin/pickle \
    && chmod +x /usr/local/bin/pickle \
    && docker-php-ext-configure intl \
    && docker-php-ext-install pdo_mysql gmp opcache intl gd\
    && apk del -f .build-deps

COPY --from=composer:2.2 /usr/bin/composer /usr/bin/composer
COPY php/conf.d/development/. $PHP_INI_DIR/conf.d/

ENV COMPOSER_HOME /tmp/composer

RUN mkdir -p /tmp/composer \
    && chmod -R 777 /tmp/composer \
    && cp $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini \
    && apk add --no-cache --update openssh git linux-headers sudo \
    && apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
    && pickle install xdebug --defaults \
    && docker-php-ext-enable xdebug  \
    && apk del -f .build-deps
